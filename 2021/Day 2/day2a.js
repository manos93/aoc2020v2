var fs = require("fs");
var text = fs.readFileSync("./2021/Day 2/input_day_2.txt").toString('utf-8');
var textByLine = text.split("\n");

let x = 0;
let y = 0;

textByLine.forEach((item, index) => {
    if (item.split(" ")[0] === 'forward') {
        x = x + parseInt(item.split(" ")[1], 10);
    } else if (item.split(" ")[0] === 'down') {
        y = y + parseInt(item.split(" ")[1], 10);
    } else if (item.split(" ")[0] === 'up') {
        y = y - parseInt(item.split(" ")[1], 10);
    } 
});

console.log(x);
console.log(y);
console.log(x*y);
