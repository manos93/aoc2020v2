var fs = require("fs");
var text = fs.readFileSync("./2021/Day 2/input_day_2.txt").toString('utf-8');
var textByLine = text.split("\n");

let x = 0;
let y = 0;
let aim = 0;
textByLine.forEach((item, index) => {
    if (item.split(" ")[0] === 'forward') {
        x = x + parseInt(item.split(" ")[1], 10);
        y = y + (aim * parseInt(item.split(" ")[1], 10));
    } else if (item.split(" ")[0] === 'down') {
        aim = aim + parseInt(item.split(" ")[1], 10);
    } else if (item.split(" ")[0] === 'up') {
        aim = aim - parseInt(item.split(" ")[1], 10);
    } 
});

console.log(x);
console.log(y);
console.log(aim);
console.log(x*y);
