var fs = require("fs");
var text = fs.readFileSync("./2021/Day 1/input_day_1.txt").toString('utf-8');
var textByLine = text.split("\n").map(function(item) {
  return parseInt(item, 10);
});
let inc_num = 0;
textByLine.forEach((item, index) => {
  if (index > 0 && textByLine[index-1] < item) {
    inc_num++;
  }
  console.log(item + " "  + ((index > 0 && textByLine[index-1] < item) ? "inc" : "desc"));
});

console.log(inc_num);