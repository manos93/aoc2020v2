var fs = require("fs");
var text = fs.readFileSync("./2021/Day 3/input_day_3.txt").toString('utf-8');
// var text = fs.readFileSync("./2021/test_input.txt").toString('utf-8');
var textByLine = text.split("\n");

let numberOfReports = textByLine.length;
let numberOfCollumn = textByLine[0].length-1;
let sumsOfOnes = new Array(numberOfCollumn).fill(0);

textByLine.forEach((item, index) => {
    for (let i = 0; i < numberOfCollumn; i++) {
        sumsOfOnes[i] = item.split("")[i] === "1"  ? sumsOfOnes[i] + 1 : sumsOfOnes[i];
    }
});

let mostCommonBit = new Array(numberOfCollumn).fill(0);
let lesCommonBit = new Array(numberOfCollumn).fill(1);

for (let i = 0; i < numberOfCollumn; i++) {
     if (numberOfReports - parseInt(sumsOfOnes[i], 10) < ~~(numberOfReports/2)) {
        mostCommonBit[i] = 1;
        lesCommonBit[i] = 0;
     }
}

let gammaRate = parseInt(mostCommonBit.join(''), 2);
let epsilonRate = parseInt(lesCommonBit.join(''), 2);

console.log('Power consumption of the submarine: ' + gammaRate * epsilonRate);